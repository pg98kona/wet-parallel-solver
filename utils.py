from typing import Union

from pulp import LpProblem, LpVariable, LpInteger, LpMinimize, PULP_CBC_CMD


def provide_pulp_variables(amount: int) -> tuple[LpProblem, list[LpVariable], list[LpVariable], list[LpVariable]]:
    c = [LpVariable(f"C_{l}", 0, cat=LpInteger) for l in range(amount)]
    t = [LpVariable(f"T_{l}", 0, cat=LpInteger) for l in range(amount)]
    e = [LpVariable(f"E_{l}", 0, cat=LpInteger) for l in range(amount)]

    problem = LpProblem(sense=LpMinimize)
    problem.solver = PULP_CBC_CMD(msg=False)

    return problem, c, t, e


def get_interval_intersect(i1start: int, i1end: int, i2start: int, i2end: int) -> Union[tuple[int, int], None]:
    result = (max(i1start, i2start), min(i1end, i2end))
    if result[0] < result[1]:
        return result
    return None
