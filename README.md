# WET Parallel Solver

This tool is able to give a solution for the weighted earliness-tardiness parallel machine problem following the paper *"A multi-agent system for the weighted earliness tardiness parallel machine problem"* (2013) by S. Polyakovskiy, R. M'Hallah.

you can edit the start parameters in `main.py` to set limits of values or even predefine values as detailed in the paper. You can then execute the script by
```bash
python3 main.py  
```  
You can optionally enable randomization of values by passing the option `--use-random`.

For questions, consult me by my email or creating an issue in this repository.