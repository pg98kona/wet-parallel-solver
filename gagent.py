from typing import Union

import utils
from iagent import IAgent


class GAgent:
    state: int

    def __init__(self, i: int, j: int, manager):
        self.J: list[IAgent] = []
        self.j = j
        self.i = i
        self.manager = manager
        self.completion = manager.d[j]
        self.add_job_at_index(j, 0)
        self.state = manager.get_new_state(i)

    @property
    def WET(self):
        return sum(job.get_cost_at(job.actual_completion) for job in self.J)

    @property
    def C(self):
        return self.J[-1].C

    @property
    def S(self):
        return self.J[0].S

    def __lt__(self, other):
        return self.C < other.C

    def __str__(self):
        return f"{len(self.J)} jobs on {self.i} in Interval {self.S} - {self.C}"

    def add_job_at_index(self, j: int, y: int):
        job: IAgent = self.manager.job_agents[j]
        job.is_busy = True
        job.machine = self.i
        if y == len(self.J):
            if y == 0:
                job.actual_completion = self.manager.d[j]
            else:
                job.actual_completion = self.J[y - 1].actual_completion + 1
            self.J.append(job)
        else:
            for other_job in self.manager.get_jobs_in_machine(self.i):
                other_job: IAgent
                if other_job.C > self.J[y].C:
                    other_job.actual_completion += 1
            job.actual_completion = self.J[y].S
            self.J.insert(y, job)
        self.manager.reschedule_machine(self.i, self)
        self.manager.n.remove(j)

    def get_intersection(self, j: int) -> Union[tuple[int, int], None]:
        """
        :param j: Job whose ideal interval is intersected with this G-Agents total interval
        :return: (start of intersection, end of intersection) if there is an intersection, else None
        """
        due = self.manager.d[j]
        duration = self.manager.p[self.i, j]
        return utils.get_interval_intersect(self.S, self.C, due - duration, due)

    def local_search(self):
        pairs = {(l, k) for l in range(len(self.J)) for k in range(len(self.J)) if l < k}
        other_agent_on_machine = self.manager.g_by_machine[self.i]

        agents_before = [agent for agent in other_agent_on_machine if agent.C <= self.S]
        jobs_before = list()
        for agent in agents_before:
            jobs_before.extend(agent.J)
        jobs_before.sort()

        agents_after = [agent for agent in other_agent_on_machine if agent.S >= self.C]
        jobs_after = list()
        for agent in agents_after:
            jobs_after.extend(agent.J)
        jobs_after.sort()

        for l, k in pairs:
            j_lk = list()
            for index in range(l + 1, k):
                j_lk.append(self.J[index])
            if l + 1 == k:
                orders = [(l, k), (k, l)]
            else:
                orders = [(l, j_lk, k),
                          (k, j_lk, l),
                          (j_lk, k, l),
                          (k, l, j_lk)]
            results = dict()
            for index, order in enumerate(orders):
                test_order = list(jobs_before)
                for part in order:
                    if type(part) == int:
                        test_order.append(self.J[part])
                    else:
                        test_order.extend(part)
                test_order += jobs_after
                results[index] = self.manager.evaluate_order_of_machine(self.i, test_order)[0]
            best_order = min(results, key=results.get)
            if best_order == 0:
                continue
            new_J = list()
            for part in orders[best_order]:
                if type(part) == int:
                    new_J.append(self.J[part])
                else:
                    new_J.extend(part)
            time = self.C
            self.J = new_J
            for job in reversed(self.J):
                job.actual_completion = time
                time -= 1
            self.manager.reschedule_machine(self.i)
            self.local_search()
            return

    def group_formation(self, a: list[int], b: list[int]) -> bool:
        """
        :param a: Should only contain a single element. Used for "Pass by Reference"
        :param b: Should only contain a single element. Used for "Pass by Reference"
        :return: ?
        """
        a[0] = 0
        b[0] = 0
        v = {f: self.get_intersection(f) for f in self.manager.n if self.get_intersection(f)}
        v_sorted: list[int] = sorted(v.keys(), key=lambda key: v.get(key)[1] - v.get(key)[0])
        for j in v_sorted:
            flag = self.manager.job_agents[j].group_joining(a, b, self)
            if flag:
                self.add_job_at_index(j, self.manager.job_agents[j].y)
                self.local_search()
                return True
        return False
