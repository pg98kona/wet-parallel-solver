import sys
from typing import Union

import pulp as pulp

import utils


class IAgent:
    _due: int
    j: int
    is_busy: bool
    actual_completion: int
    has_received_attachment_offer: bool
    y: int
    state: int
    inc_of_last: Union[int, None]

    def __init__(self, j: int, manager):
        self.j = j
        self.manager = manager
        self.is_busy = False
        self.actual_completion = -1
        self.has_received_attachment_offer = False
        self.machine = -1
        self.y = -1
        self.state = 0
        self.inc_of_last = -1
        self._due = self.manager.d[self.j]
        self.g_agent_last_attachment_offer = None

    def __str__(self):
        result = f"Job due: {self._due}"
        if self.actual_completion != -1:
            result += f" | Job Period: {self.S} - {self.C}"
        return result

    @property
    def C(self):
        return self.actual_completion

    @property
    def S(self):
        if self.machine != -1:
            return self.actual_completion - self.manager.p[self.machine, self.j]
        else:
            return -1

    def __lt__(self, other):
        return self.C < other.C

    def get_cost_at(self, time: int = None) -> int:
        if time is None:
            time = self.actual_completion
        if time < self._due:
            return (self._due - time) * self.manager.alpha[self.j]
        elif time > self._due:
            return (time - self._due) * self.manager.beta[self.j]
        return 0

    def compute_minimal_wet(self, g_agent, upper_bound: int) -> int:
        M = sys.maxsize / 2
        alpha = self.manager.alpha
        beta = self.manager.beta
        i = g_agent.i
        n = sorted(list(self.manager.get_jobs_in_machine(i)))
        all_jobs = list(n)
        all_jobs.append(self)
        all_jobs = [key for job in all_jobs for key, value in self.manager.job_agents.items() if value == job]
        f = len(n)
        p = {j: self.manager.p[i, all_jobs[j]] for j in range(len(all_jobs))}
        d = {j: self.manager.d[all_jobs[j]] for j in range(len(all_jobs))}

        big_y = list(range(len(g_agent.J) + 1))

        j_minus = None
        for i in range(1, len(n)):
            if n[i] == g_agent.J[0]:
                j_minus = i - 1
                break
        j_plus = None
        for i in range(1, len(n)):
            if n[i - 1] == g_agent.J[-1]:
                j_plus = i
                break

        problem, c, t, e = utils.provide_pulp_variables(len(all_jobs))

        x = [pulp.LpVariable(f"x_{y}", lowBound=0, upBound=1, cat=pulp.const.LpInteger) for y in big_y]

        problem += pulp.lpSum(alpha[all_jobs[l]] * e[l] + beta[all_jobs[l]] * t[l] for l in range(len(all_jobs)))

        for l in range(len(n)):
            problem += c[l + 1] - c[l] >= p[l + 1]
            problem += c[l + 1] + e[l + 1] - t[l + 1] == d[l + 1]
        for y in big_y:
            if y != 0 and j_minus is not None:
                problem += c[f] - c[j_minus] - M * x[y] >= p[f] - M
            if y != len(g_agent.J) and j_plus is not None:
                problem += c[j_plus] - c[f] - M * x[y] >= p[j_plus] - M
        problem += pulp.lpSum(x) == 1
        problem += c[0] >= p[0]
        problem += c[0] + e[0] - t[0] == d[0]

        problem += c[f] >= p[f]

        status = problem.solve()
        assert status == 1

        vals_t = [int(pulp.value(part)) for part in t]
        vals_e = [int(pulp.value(part)) for part in e]
        vals_x = [int(pulp.value(part)) for part in x]

        z = sum(alpha[all_jobs[l]] * vals_e[l] + beta[all_jobs[l]] * vals_t[l] for l in range(len(all_jobs)))

        if z < upper_bound:
            self.y = next(index for index, place in enumerate(vals_x) if place == 1)
            return z
        return -1

    def group_joining(self, a: list[int], b: list[int], g_agent) -> bool:
        if not self.has_received_attachment_offer:
            self.has_received_attachment_offer = True
            fractal_m: set[int] = {i for i in self.manager.get_free_machines_for_zero_wet(self.j) if i != g_agent.i}
            if fractal_m:
                fastest_machine = self.manager.get_fastest_machine(self.j, fractal_m)
                self.manager.create_g_agent(fastest_machine, self.j)
                return False
        if self.inc_of_last >= 0 and self.state == self.g_agent_last_attachment_offer.state:
            if g_agent != self.g_agent_last_attachment_offer:
                wet = self.compute_minimal_wet(g_agent,
                                               self.manager.get_wet_of_machine(g_agent.i) + self.inc_of_last)
            else:
                wet = self.manager.get_wet_of_machine(g_agent.i) + self.inc_of_last
        else:
            wet = self.compute_minimal_wet(g_agent, sys.maxsize)
        if wet >= 0:
            self.inc_of_last = wet - self.manager.get_wet_of_machine(g_agent.i)
            self.g_agent_last_attachment_offer = g_agent
        else:
            self.state = g_agent.state
            self.g_agent_last_attachment_offer = g_agent
            return False
        for i_strich in self.manager.machines:
            for j_strich in range(1, len(self.manager.g_by_machine[i_strich]) + 1):
                other_g_agent = self.manager.g_by_machine[i_strich][j_strich - 1]
                if g_agent != other_g_agent and other_g_agent.get_intersection(self.j):
                    wet = self.compute_minimal_wet(other_g_agent,
                                                   self.manager.get_wet_of_machine(i_strich) + self.inc_of_last)
                    if wet >= 0:
                        self.state = self.manager.g_by_machine[i_strich][j_strich - 1].state
                        self.inc_of_last = wet - self.manager.get_wet_of_machine(i_strich)
                        a[0] = i_strich
                        b[0] = j_strich - 1
                        self.g_agent_last_attachment_offer = g_agent
                        return False
        return True
