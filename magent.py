import math
import sys

import pulp

import utils
from gagent import GAgent
from iagent import IAgent


class MAgent:
    phis: dict[int, dict[int, float]]
    f_star: dict[int, int]
    g_by_machine: dict[int, list[GAgent]]
    g: dict[tuple[int, int], GAgent]

    def __init__(self, n: list[int], d: dict[int, int], m: set[int],
                 p: dict[tuple[int, int], int], alpha: dict[int, int], beta: dict[int, int]):
        self.n = n
        self.d = d
        self.p = {(i, j): min(cost, d[j]) for (i, j), cost in p.items()}
        self.alpha = alpha
        self.beta = beta
        self.job_agents: dict[int, IAgent] = {j: IAgent(j, self) for j in n}
        self.machines = m
        self.m = len(self.machines)
        self.redirect = False
        self.g = dict()
        self.g_by_machine = {i: list() for i in self.machines}
        self.k = 1

        self.phis = {
            1: {f: self.phi(f, self.n, self.k, self.d) for f in self.n}
        }
        self.f_star = {
            1: max(self.phis[1], key=self.phis[1].get)
        }
        i_strich = self.get_fastest_machine(self.f_star[1])

        self.create_g_agent(i_strich, self.f_star[1])

        self.generate_partial_solution()

        self.complete_solution()

        self.print_solution()

    def merge_agents_on_machine(self, i: int):
        g_agents = sorted(self.g_by_machine[i])
        for index in range(len(g_agents) - 1):
            if g_agents[index].C == g_agents[index + 1].S:
                print(f"Merging Agents ({g_agents[index]}) and ({g_agents[index + 1]})")
                self.redirect = True
                g_agents[index + 1].J.extend(g_agents[index].J)
                g_agents[index + 1].J.sort()
                g_agents[index].J.clear()
                self.g_by_machine[i].remove(g_agents[index])
                self.merge_agents_on_machine(i)
                return

    def get_new_state(self, i: int) -> int:
        states = [g.state for g in self.g_by_machine[i]]
        for number in range(len(states) + 1):
            if number not in states:
                return number
        return -1

    def update_states_on_machine(self, i: int):
        for g_agent in self.g_by_machine[i]:
            g_agent.state += 1

    def phi(self, f: int, n: list[int], k: int, d: dict[int, int]) -> float:
        def exp(f: int, fx: int) -> float:
            return math.exp(-(((d[f] - d[fx]) ** 2) / ((0.5 * r) ** 2)))

        r = 1
        if k == 1:
            return sum(exp(f, f_dash) for f_dash in n)
        k -= 1
        return self.phis[k][f] - self.phis[k][self.f_star[k]] * exp(f, self.f_star[k])

    def create_g_agent(self, i: int, j: int):
        new_g_agent = GAgent(i, j, self)
        self.g_by_machine[i].append(new_g_agent)
        self.g[i, len(self.g_by_machine[i])] = new_g_agent
        self.g_by_machine[i] = sorted(self.g_by_machine[i], key=lambda g_agent: g_agent.C)

        print(f"Created G-Agent ({new_g_agent})")

        self.redirect = True

    def evaluate_order_of_machine(self, i: int, n_i: list[IAgent]) -> tuple[int, list[int]]:
        all_jobs = [key for job in n_i for key, value in self.job_agents.items() if value == job]
        p = {j: self.p[i, all_jobs[j]] for j in range(len(all_jobs))}
        d = {j: self.d[all_jobs[j]] for j in range(len(all_jobs))}

        problem, c, t, e = utils.provide_pulp_variables(len(all_jobs))

        problem += pulp.lpSum(
            self.alpha[all_jobs[l]] * e[l] + self.beta[all_jobs[l]] * t[l] for l in range(len(all_jobs)))
        problem += c[0] >= p[0]
        problem += c[0] + e[0] - t[0] == d[0]
        for l in range(len(all_jobs) - 1):
            problem += c[l + 1] - c[l] >= p[l + 1]
            problem += c[l + 1] + e[l + 1] - t[l + 1] == d[l + 1]

        status = problem.solve()
        assert status == 1

        vals_c = [int(pulp.value(part)) for part in c]
        vals_t = [int(pulp.value(part)) for part in t]
        vals_e = [int(pulp.value(part)) for part in e]

        wet = sum(
            self.alpha[all_jobs[l]] * vals_e[l] + self.beta[all_jobs[l]] * vals_t[l] for l in range(len(all_jobs)))

        return wet, vals_c

    def reschedule_machine(self, i: int, new_agent: GAgent = None):
        n = sorted(list(self.get_jobs_in_machine(i)))
        if new_agent:
            n = sorted(n + new_agent.J)

        _, vals_c = self.evaluate_order_of_machine(i, n)

        for index, job in enumerate(n):
            job.actual_completion = vals_c[index]

        self.update_states_on_machine(i)

    def get_fastest_machine(self, j: int, available_machines: set[int] = None) -> int:
        if available_machines is None:
            available_machines = self.machines
        min_duration = min(self.p[(i, j)] for i in self.machines if i in available_machines)
        return next(i for i in available_machines if self.p[(i, j)] == min_duration)

    def get_jobs_in_machine(self, i: int) -> list[IAgent]:
        jobs = list()
        for g_agent in self.g_by_machine[i]:
            jobs.extend(g_agent.J)
        return jobs

    def get_free_machines_for_zero_wet(self, j: int) -> set[int]:
        result = set()
        due = self.d[j]
        for i in self.machines:
            duration = self.p[i, j]
            if duration > due:
                continue
            if not self.g_by_machine[i]:
                result.add(i)
                continue
            jobs = self.get_jobs_in_machine(i)
            jobs_with_intersect = [job for job in jobs if
                                   utils.get_interval_intersect(job.S, job.C, due - duration, due)]
            if jobs_with_intersect:
                continue
            result.add(i)
        return result

    def get_wet_of_machine(self, i: int) -> int:
        jobs = self.get_jobs_in_machine(i)
        return sum(job.get_cost_at(job.actual_completion) for job in jobs)

    def generate_partial_solution(self):
        while True:
            self.phis[self.k + 1] = {f: self.phi(f, self.n, self.k + 1, self.d) for f in self.n}
            self.f_star[self.k + 1] = max(self.phis[self.k + 1], key=self.phis[self.k + 1].get)
            if self.phis[self.k + 1][self.f_star[self.k + 1]] >= 1:
                fraktur_m = self.get_free_machines_for_zero_wet(self.f_star[self.k + 1])
                if fraktur_m and len(fraktur_m) < self.m:
                    i_strich = self.get_fastest_machine(self.f_star[self.k + 1], fraktur_m)

                    self.create_g_agent(i_strich, self.f_star[self.k + 1])
                self.k += 1
            else:
                break

    def complete_solution(self):
        while self.n:
            print(self.n)
            self.redirect = False
            flag = False
            for i in self.machines:
                for agent in self.g_by_machine[i]:
                    a = [0]
                    b = [0]
                    f1 = agent.group_formation(a, b)
                    if f1:
                        print(f"Try merging G-Agents on {i}")
                        self.merge_agents_on_machine(i)
                        print("Finished merging")
                        self.steepest_descent()
                    flag |= f1
                    while self.redirect and a[0] >= 1 and b[0] >= 1:
                        if b[0] <= len(self.g_by_machine[a[0]]):
                            c = a[0]
                            f2 = self.g_by_machine[a[0]][b[0]].group_formation(a, b)
                            if f2:
                                print(f"Try merging G-Agents on Machine {c}")
                                self.merge_agents_on_machine(c)
                                self.steepest_descent()
                        else:
                            break
            if not flag:
                for l in [j for j in self.n if not self.job_agents[j].is_busy]:
                    i_strich = self.get_fastest_machine(l)

                    self.create_g_agent(i_strich, l)
                    break

    def steepest_descent(self):
        for i, i_strich in ((m2, m_2) for m2 in self.machines for m_2 in self.machines if m2 != m_2):
            for g_agent, g_agent_strich in ((g1, g2) for g1 in self.g_by_machine[i] for g2 in
                                            self.g_by_machine[i_strich] if
                                            utils.get_interval_intersect(g1.S, g1.C, g2.S, g2.C)):
                self.job_exchanging(g_agent, g_agent_strich)

    def job_exchanging(self, g1: GAgent, g2: GAgent):
        # xi can be anything from 0.5 to 2.0
        xi = 1.0
        omega = self.get_wet_of_machine(g1.i) + self.get_wet_of_machine(g2.i)
        temp_js: list = [None for _ in range(len(g1.J))]
        temp_js_strich: list = [None for _ in range(len(g2.J))]

        jobs_before_g1 = sorted(job for job in self.get_jobs_in_machine(g1.i) if job.C <= g1.S)
        jobs_after_g1 = sorted(job for job in self.get_jobs_in_machine(g1.i) if job.S >= g1.C)
        jobs_before_g2 = sorted(job for job in self.get_jobs_in_machine(g2.i) if job.C <= g2.S)
        jobs_after_g2 = sorted(job for job in self.get_jobs_in_machine(g2.i) if job.S >= g2.C)

        ns = dict()
        ns_strich = dict()
        wets = dict()

        for k in range(len(g1.J)):
            for l in range(len(g2.J)):
                if k >= len(g1.J) or l >= len(g2.J):
                    continue
                if k > 0 and l > 0:
                    flag = not ((self.d[l] + xi * self.p[g2.i, l] <= self.d[k] - (1 + xi) * self.p[g1.i, k]) or (
                            self.d[k] + xi * self.p[g1.i, k] <= self.d[l] - (1 + xi) * self.p[g2.i, l]))
                else:
                    flag = True
                if (k > 0 or l > 0) and flag:
                    temp_js[k] = list(g1.J)
                    if k > 0 and l > 0:
                        temp_js[k][k] = g2.J[l]
                    if k == 0 and l > 0:
                        j_l = g2.J[l]
                        due_difference = {
                            next(y for y, job in enumerate(g1.J) if job == j_y): abs(self.d[j_l.j] - self.d[j_y.j]) for
                            j_y in g1.J}
                        p = min(due_difference, key=due_difference.get)
                        if p == len(temp_js[k]):
                            temp_js[k].append(j_l)
                        else:
                            temp_js[k].insert(p + 1, j_l)
                    if k > 0 and l == 0:
                        temp_js[k].remove(temp_js[k][k])
                    timing = self.timing(temp_js[k], g1.S, g1.C, g1.i)
                    if l > 0:
                        temp_js[k] = self.repositioning(temp_js[k], g2.J[l], g1, timing)
                    if self.timing(temp_js[k], g1.S, g1.C, g1.i) == sys.maxsize:
                        break
                    ns[k] = jobs_before_g1 + temp_js[k] + jobs_after_g1
                    wets[g1.i, k] = self.evaluate_order_of_machine(g1.i, ns[k])[0]
                    if omega <= wets[g1.i, k]:
                        break

                    temp_js_strich[l] = list(g2.J)
                    if k > 0 and l > 0:
                        temp_js_strich[l][l] = g1.J[k]
                    if k > 0 and l == 0:
                        j_k = g1.J[k]
                        due_difference = {
                            next(y for y, job in enumerate(g2.J) if job == j_y): abs(self.d[j_k.j] - self.d[j_y.j]) for
                            j_y in g2.J}
                        p = min(due_difference, key=due_difference.get)
                        if p == len(temp_js_strich[l]):
                            temp_js_strich[l].append(j_k)
                        else:
                            temp_js_strich[l].insert(p + 1, j_k)
                    if k == 0 and l > 0:
                        temp_js_strich[l].remove(temp_js_strich[l][l])
                    timing = self.timing(temp_js_strich[l], g2.S, g2.C, g2.i)
                    if k > 0:
                        temp_js_strich[l] = self.repositioning(temp_js_strich[l], g1.J[k], g2, timing)
                    if self.timing(temp_js_strich[l], g2.S, g2.C, g2.i) == sys.maxsize:
                        break
                    ns_strich[l] = jobs_before_g2 + temp_js_strich[l] + jobs_after_g2
                    wets[g2.i, l] = self.evaluate_order_of_machine(g2.i, ns_strich[l])[0]
                    if omega <= wets[g1.i, k] + wets[g2.i, l]:
                        break
                    print(f"Job exchange between Machines {g1.i} and {g2.i}")
                    g1.J = temp_js[k]
                    g2.J = temp_js_strich[l]
                    omega = wets[g1.i, k] + wets[g2.i, l]

    def repositioning(self, j_k: list[IAgent], j_l: IAgent, g: GAgent, upper_bound: int) -> list[IAgent]:
        j_k_temp = list(j_k)
        j_k_temp.remove(j_l)
        all_permutations = list()
        for i in range(len(j_k_temp) + 1):
            permutation = list(j_k_temp)
            if i == len(j_k_temp):
                permutation.append(j_l)
            else:
                permutation.insert(i, j_l)
            if permutation != j_k:
                all_permutations.append(permutation)
        all_permutations.append(j_k)

        results = {index: self.timing(permutation, g.S, g.C, g.i) for index, permutation in enumerate(all_permutations)}

        best = min(results, key=results.get)
        wet = results[best]

        if wet < upper_bound:
            return all_permutations[best]
        return list()

    def timing(self, j_k: list[IAgent], start: int, end: int, i: int) -> int:
        if not j_k:
            return sys.maxsize
        problem, c, t, e = utils.provide_pulp_variables(len(j_k))
        problem += pulp.lpSum(self.alpha[j.j] * e[y] + self.beta[j.j] * t[y] for y, j in enumerate(j_k))

        problem += c[0] >= self.p[i, j_k[0].j] + start
        problem += c[len(j_k) - 1] <= end
        problem += c[0] - self.d[j_k[0].j] == t[0] - e[0]

        for index in range(1, len(j_k)):
            problem += c[index] - c[index - 1] >= self.p[i, j_k[index].j]
            problem += c[index] - self.d[j_k[index].j] == t[index] - e[index]

        status = problem.solve()
        if status != 1:
            return sys.maxsize

        vals_t = [int(pulp.value(part)) for part in t]
        vals_e = [int(pulp.value(part)) for part in e]

        wet = sum(
            self.alpha[job.j] * vals_e[l] + self.beta[job.j] * vals_t[l] for l, job in enumerate(j_k))

        return wet

    def print_solution(self):
        print("")
        result = ""
        for i in self.machines:
            result += f"Machine {i}:\n"
            for job in sorted(self.get_jobs_in_machine(i), key=lambda j: j.S):
                result += f"\t{job.j}: {job.S}-{job.C} /{self.d[job.j]} -> {job.get_cost_at()}\n"
        print(result)
