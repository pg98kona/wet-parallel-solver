import random

import click

from magent import MAgent


@click.command()
@click.option("--use-random", is_flag=True, default=False)
def test_run(use_random):
    if use_random:
        MACHINE_COUNT = 6
        JOB_COUNT = 75
        DUE_INTERVAL = (1, 40)
        DURATION_INTERVAL = (1, 8)

        n = list(range(1, JOB_COUNT + 1))
        d = {j: random.randint(DUE_INTERVAL[0], DUE_INTERVAL[1]) for j in n}
        machines = set(range(1, MACHINE_COUNT + 1))
        p = {(i, j): random.randint(DURATION_INTERVAL[0], DURATION_INTERVAL[1]) for i in machines for j in n}
    else:
        n = list(range(1, 21))
        d = {
            1: 20, 2: 5, 3: 4, 4: 11, 5: 10, 6: 8, 7: 3, 8: 3, 9: 17, 10: 16, 11: 18, 12: 1, 13: 18, 14: 5, 15: 17,
            16: 2, 17: 19, 18: 4, 19: 3, 20: 9
        }
        machines = {1, 2, 3, 4}
        p = {
            (1, 1): 7, (1, 2): 6, (1, 3): 4, (1, 4): 1, (1, 5): 7, (1, 6): 4, (1, 7): 8, (1, 8): 2, (1, 9): 4,
            (1, 10): 6, (1, 11): 8, (1, 12): 1, (1, 13): 8, (1, 14): 7, (1, 15): 8, (1, 16): 1, (1, 17): 7, (1, 18): 6,
            (1, 19): 6, (1, 20): 1, (2, 1): 1, (2, 2): 3, (2, 3): 2, (2, 4): 5, (2, 5): 7, (2, 6): 3, (2, 7): 2,
            (2, 8): 5, (2, 9): 7, (2, 10): 3, (2, 11): 2, (2, 12): 4, (2, 13): 7, (2, 14): 8, (2, 15): 4, (2, 16): 4,
            (2, 17): 8, (2, 18): 5, (2, 19): 4, (2, 20): 8, (3, 1): 4, (3, 2): 7, (3, 3): 1, (3, 4): 1, (3, 5): 3,
            (3, 6): 1, (3, 7): 4, (3, 8): 1, (3, 9): 4, (3, 10): 3, (3, 11): 1, (3, 12): 4, (3, 13): 1, (3, 14): 6,
            (3, 15): 7, (3, 16): 3, (3, 17): 3, (3, 18): 4, (3, 19): 3, (3, 20): 5, (4, 1): 3, (4, 2): 2, (4, 3): 8,
            (4, 4): 7, (4, 5): 6, (4, 6): 7, (4, 7): 3, (4, 8): 7, (4, 9): 2, (4, 10): 3, (4, 11): 8, (4, 12): 2,
            (4, 13): 8, (4, 14): 4, (4, 15): 6, (4, 16): 4, (4, 17): 1, (4, 18): 5, (4, 19): 3, (4, 20): 8
        }
    manager = MAgent(n=n, d=d, p=p, m=machines, alpha={j: 2 for j in n}, beta={j: 3 for j in n})


if __name__ == '__main__':
    test_run()
